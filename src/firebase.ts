import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import {
    GoogleAuthProvider,
    getAuth,
    signInWithPopup,
    signInWithEmailAndPassword,
    createUserWithEmailAndPassword,
    sendPasswordResetEmail,
    signOut,
  } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyAwL7pCoMDDW1yMhW7SSW9kKSQ_unqTjWA",
  authDomain: "simple-app-4f689.firebaseapp.com",
  projectId: "simple-app-4f689",
  storageBucket: "simple-app-4f689.appspot.com",
  messagingSenderId: "901566223406",
  appId: "1:901566223406:web:c86c6470e3136a6283bbbf",
  measurementId: "G-QECFH949K1"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

const logInWithEmailAndPassword = async (email, password) => {
  try {
    const res = await signInWithEmailAndPassword(auth, email, password);
    return res
  } catch (err) {
    console.error(err);
    alert(err.message);
  }
};

const registerWithEmailAndPassword = async (name, email, password) => {
    console.log(app);
  try {
    const res = await createUserWithEmailAndPassword(auth, email, password);
    alert("Success!!!")
  } catch (err) {
    alert(err.message);
  }
};

const sendPasswordReset = async (email) => {
  try {
    await sendPasswordResetEmail(auth, email);
    alert("Password reset link sent!");
  } catch (err) {
    console.error(err);
    alert(err.message);
  }
};

const logout = () => {
  signOut(auth);
};

export {
  auth,
  logInWithEmailAndPassword,
  registerWithEmailAndPassword,
  sendPasswordReset,
  logout,
};