import React, { useEffect } from 'react'
import type { ReactElement } from 'react'
import Head from 'next/head'
import BaseButton from '../components/BaseButton'
import CardBox from '../components/CardBox'
import SectionFullScreen from '../components/SectionFullScreen'
import LayoutGuest from '../layouts/Guest'
import { Field, Form, Formik, useFormik } from 'formik'
import FormField from '../components/FormField'
import FormCheckRadio from '../components/FormCheckRadio'
import BaseDivider from '../components/BaseDivider'
import BaseButtons from '../components/BaseButtons'
import { useRouter } from 'next/router'
import { getPageTitle } from '../config'
import { logInWithEmailAndPassword } from '../firebase'

export default function Error() {
  const router = useRouter()

  useEffect(() => {
    localStorage.removeItem("access_token");
  })

  const formik = useFormik({ 
    initialValues: { email: 'email', password: 'password', remember: true }, 
    onSubmit:async (values) => { }, 
  }); 

  const handleAuth = async () => {
    try {
      const response = await fetch('http://localhost:8080/v1/passport/auth', {
        method: 'POST', // or 'PUT'
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'Bearer ' + await localStorage.getItem("access_token"),
        },
      })

    } catch (error) {
      console.error('Error:', error)
    }
  }


  const handleSubmit = async (values) => {
    const res = await logInWithEmailAndPassword(values.email, values.password);
    if (res.user != null) {
      const idtoken = await res.user.getIdToken()
      localStorage.setItem("access_token", idtoken);
      handleAuth();
      router.push('/todo')
    }
  }

  return (
    <>
      <Head>
        <title>{getPageTitle('Login')}</title>
      </Head>

      <SectionFullScreen bg="purplePink">
        <CardBox className="w-11/12 md:w-7/12 lg:w-6/12 xl:w-4/12 shadow-2xl">
          <Formik
            initialValues={{ email: 'email', password: 'password', remember: true }}
            onSubmit={(values) => handleSubmit(values)}
          >
            <Form>
              <FormField label="Email" help="Please enter your login">
                <Field name="email" />
              </FormField>

              <FormField label="Password" help="Please enter your password">
                <Field name="password" type="password" />
              </FormField>

              <FormCheckRadio type="checkbox" label="Remember">
                <Field type="checkbox" name="remember" />
              </FormCheckRadio>

              <BaseDivider />

              <BaseButtons>
                <BaseButton type="submit" label="Login" color="info" />
                <BaseButton href="/register" label="Register" color="info" outline />
                <BaseButton href="/forgot-password" label="Forgot Password" color="info" outline />
              </BaseButtons>
            </Form>
          </Formik>
        </CardBox>
      </SectionFullScreen>
    </>
  )
}

Error.getLayout = function getLayout(page: ReactElement) {
  return <LayoutGuest>{page}</LayoutGuest>
}
