import React from 'react'
import type { ReactElement } from 'react'
import Head from 'next/head'
import BaseButton from '../components/BaseButton'
import CardBox from '../components/CardBox'
import SectionFullScreen from '../components/SectionFullScreen'
import LayoutGuest from '../layouts/Guest'
import { Field, Form, Formik } from 'formik'
import FormField from '../components/FormField'
import FormCheckRadio from '../components/FormCheckRadio'
import BaseDivider from '../components/BaseDivider'
import BaseButtons from '../components/BaseButtons'
import { useRouter } from 'next/router'
import { getPageTitle } from '../config'
import { sendPasswordReset } from '../firebase'

export default function Error() {
  const router = useRouter()

  const handleSubmit = async (values) => {
    const res = await sendPasswordReset(values.email);
    console.log(res)
    // if (res.user != null) {
    //   const idtoken = await res.user.getIdToken()
    //   localStorage.setItem("access_token", idtoken);
    router.push('/login')
    // }
  }

  return (
    <>
      <Head>
        <title>{getPageTitle('Forgot Password')}</title>
      </Head>

      <SectionFullScreen bg="purplePink">
        <CardBox className="w-11/12 md:w-7/12 lg:w-6/12 xl:w-4/12 shadow-2xl">
          <Formik
            initialValues={{ email: 'email@gmail.com' }}
            onSubmit={(values) => handleSubmit(values)}
          >
            <Form>
              <FormField label="Email" help="Please enter your email for request password">
                <Field name="email" />
              </FormField>

              <BaseDivider />

              <BaseButtons>
                <BaseButton type="submit" label="Submit" color="info" />
                <BaseButton href="/login" label="Login" color="info" outline />
                
              </BaseButtons>
            </Form>
          </Formik>
        </CardBox>
      </SectionFullScreen>
    </>
  )
}

Error.getLayout = function getLayout(page: ReactElement) {
  return <LayoutGuest>{page}</LayoutGuest>
}
