import React from 'react'
import type { ReactElement } from 'react'
import Head from 'next/head'
import BaseButton from '../components/BaseButton'
import CardBox from '../components/CardBox'
import SectionFullScreen from '../components/SectionFullScreen'
import LayoutGuest from '../layouts/Guest'
import { Field, Form, Formik, useFormik } from 'formik'
import FormField from '../components/FormField'
import FormCheckRadio from '../components/FormCheckRadio'
import BaseDivider from '../components/BaseDivider'
import BaseButtons from '../components/BaseButtons'
import { useRouter } from 'next/router'
import { getPageTitle } from '../config'
import { logInWithEmailAndPassword, registerWithEmailAndPassword } from '../firebase'

export default function Error() {
  const router = useRouter()
  const formik = useFormik({ 
    initialValues: { name: 'name', email: 'email', password: 'password', repeat_password: 'password', remember: true }, 
    onSubmit:async (values) => { 
      // // alert(JSON.stringify(values, null, 2)); 
      // if (!values.name) alert("Please enter name");
      // if (!values.email) alert("Please enter name");
      // await registerWithEmailAndPassword(values.name, values.email, values.password);
    }, 
  }); 

  const handleSubmit = async (values) => {
    if (!values.name) alert("Please enter name");
    if (!values.email) alert("Please enter name");
    await registerWithEmailAndPassword(values.name, values.email, values.password);
    router.push('/login')
  }

  return (
    <>
      <Head>
        <title>{getPageTitle('Register')}</title>
      </Head>

      <SectionFullScreen bg="purplePink">
        <CardBox className="w-11/12 md:w-7/12 lg:w-6/12 xl:w-4/12 shadow-2xl">
          <Formik
            initialValues={formik.initialValues}
            onSubmit={values => handleSubmit(values)}
          >
            <Form>
              <FormField label="Name" help="Please enter your name">
                <Field name="name"  />
              </FormField>

              <FormField label="Email" help="Please enter your email">
                <Field name="email" />
              </FormField>

              <FormField label="Password" help="Please enter your password">
                <Field name="password"  type="password" />
              </FormField>

              <FormField label="Password" help="Please enter your password">
                <Field name="repeat_password" type="password" />
              </FormField>

              <BaseDivider />

              <BaseButtons>
                <BaseButton type="submit" label="Register" color="info" />
                <BaseButton href="/login" label="Login" color="info" outline />
                <BaseButton href="/forgot-password" label="Forgot Password" color="info" outline />
              </BaseButtons>
            </Form>
          </Formik>
        </CardBox>
      </SectionFullScreen>
    </>
  )
}

Error.getLayout = function getLayout(page: ReactElement) {
  return <LayoutGuest>{page}</LayoutGuest>
}
