import {
  mdiAccount,
  mdiAccountMultiple,
  mdiAppleKeyboardOption,
  mdiBeakerQuestion,
  mdiCartOutline,
  mdiChartPie,
  mdiChartTimelineVariant,
  mdiGithub,
  mdiGreasePencil,
  mdiListStatus,
  mdiMail,
  mdiMonitorCellphone,
  mdiReload,
} from '@mdi/js'
import Head from 'next/head'
import React, { useEffect, useState } from 'react'
import type { ReactElement } from 'react'
import BaseButton from '../components/BaseButton'
import LayoutAuthenticated from '../layouts/Authenticated'
import SectionMain from '../components/SectionMain'
import SectionTitleLineWithButton from '../components/SectionTitleLineWithButton'
import CardBoxWidget from '../components/CardBoxWidget'
import { useSampleClients, useSampleTransactions } from '../hooks/sampleData'
import CardBoxTransaction from '../components/CardBoxTransaction'
import { Client, Transaction } from '../interfaces'
import CardBoxClient from '../components/CardBoxClient'
import SectionBannerStarOnGitHub from '../components/SectionBannerStarOnGitHub'
import CardBox from '../components/CardBox'
import { sampleChartData } from '../components/ChartLineSample/config'
import ChartLineSample from '../components/ChartLineSample'
import NotificationBar from '../components/NotificationBar'
import TableSampleClients from '../components/TableSampleClients'
import { getPageTitle } from '../config'
import CardBoxModal from '../components/CardBoxModal'
import { Field, Form, Formik } from 'formik'
import FormCheckRadio from '../components/FormCheckRadio'
import FormField from '../components/FormField'
import BaseButtons from '../components/BaseButtons'

const Dashboard = () => {
  const [tasks, setTasks] = useState([])
  const [taskForUpdate, setTaskForUpdate] = useState({})
  const [isModalCreateActive, setIsModalCreateActive] = useState(false)
  const [isModalUpdateActive, setIsModalUpdateActive] = useState(false)

  const handleModalCreateAction = () => {
    setIsModalCreateActive(false)
  }

  const handleModalUpdateAction = () => {
    setIsModalUpdateActive(false)
  }

  async function fetchData(name = '', date = 0) {
    const response = await fetch('http://localhost:8080/v1/tasks?'+ new URLSearchParams({
      name: name,
      date: date.toString(),
    }), {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'Bearer ' + await localStorage.getItem("access_token"),
      }
    })
    const jsonData = await response.json()
    setTasks(jsonData.data.tasks)
    console.log(jsonData)
  }

  const createTask = async (values) => {
    try {
      const response = await fetch('http://localhost:8080/v1/tasks', {
        method: 'POST', // or 'PUT'
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'Bearer ' + await localStorage.getItem("access_token"),
        },
        body: JSON.stringify({
          task: values,
        }),
      })

      const result = await response.json()
      alert('Create Success')
      fetchData()
    } catch (error) {
      console.error('Error:', error)
    }
  }

  const handleUpdate = async (values) => {
    try {
      const response = await fetch('http://localhost:8080/v1/tasks/' + values.id, {
        method: 'PUT', // or 'PUT'
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'Bearer ' + await localStorage.getItem("access_token"),
        },
        body: JSON.stringify({
          task: values,
        }),
      })

      const result = await response.json()
      alert('Update Success')
      fetchData()
    } catch (error) {
      console.error('Error:', error)
    }
  }

  const handleDelete = async (id) => {
    try {
      const response = await fetch('http://localhost:8080/v1/tasks/' + id, {
        method: 'DELETE', // or 'PUT'
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'Bearer ' + await localStorage.getItem("access_token"),
        },
      })

      const result = await response.json()
      alert('Delete Success')
      fetchData()
    } catch (error) {
      console.error('Error:', error)
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <>
      <Head>
        <title>{getPageTitle('Todo Task')}</title>
      </Head>
      <SectionMain>
        <SectionTitleLineWithButton
          icon={mdiChartTimelineVariant}
          title="Todo Task"
          main
          handleClick={() => setIsModalCreateActive(true)}
        ></SectionTitleLineWithButton>

        {/* Modal Create */}
        <CardBoxModal
          title="Create Todo"
          buttonColor="info"
          buttonLabel="Done"
          isActive={isModalCreateActive}
          onConfirm={handleModalCreateAction}
        >
          <CardBox>
            <Formik
              initialValues={{
                name: '',
                description: '',
                status: 'TASK_STATUS_INVALID',
                priority: 'PRIORIRY_INVALID',
              }}
              onSubmit={(values) => {
                createTask(values)
                console.log(values)
                handleModalCreateAction()
              }}
            >
              <Form>
                <FormField label="Name" icons={[mdiBeakerQuestion]}>
                  <Field name="name" placeholder="name" />
                </FormField>

                <FormField label="Description" icons={[mdiAppleKeyboardOption]}>
                  <Field name="description" placeholder="description" />
                </FormField>

                <FormField label="Status" icons={[mdiBeakerQuestion]}>
                  <Field as="select" name="status">
                    <option key="0" value="TASK_STATUS_INVALID">
                      TASK_STATUS_INVALID
                    </option>
                    <option key="1" value="TASK_STATUS_INPROGRESS">
                      TASK_STATUS_INPROGRESS
                    </option>
                    <option key="2" value="TASK_STATUS_COMPLETED">
                      TASK_STATUS_COMPLETED
                    </option>
                    <option key="3" value="TASK_STATUS_CANCELLED">
                      TASK_STATUS_CANCELLED
                    </option>
                  </Field>
                </FormField>

                <FormField label="Priority" icons={[mdiBeakerQuestion]}>
                  <Field as="select" name="priority">
                    <option key="0" value="PRIORIRY_INVALID">
                      PRIORIRY_INVALID
                    </option>
                    <option key="1" value="PRIORIRY_LOW">
                      PRIORIRY_LOW
                    </option>
                    <option key="2" value="PRIORIRY_MEDIUM">
                      PRIORIRY_MEDIUM
                    </option>
                    <option key="3" value="PRIORIRY_HIGH">
                      PRIORIRY_HIGH
                    </option>
                  </Field>
                </FormField>

                <BaseButtons>
                  <BaseButton type="submit" color="info" label="Submit" />
                  <BaseButton type="reset" color="info" outline label="Reset" />
                </BaseButtons>
              </Form>
            </Formik>
          </CardBox>
        </CardBoxModal>

        {/* Modal Update */}
        <CardBoxModal
          title="Update Todo"
          buttonColor="info"
          buttonLabel="Done"
          isActive={isModalUpdateActive}
          onConfirm={handleModalUpdateAction}
        >
          <CardBox>
            <Formik
              initialValues={taskForUpdate}
              onSubmit={(values) => {
                console.log(values)
                handleUpdate(values)
                console.log(values)
                handleModalUpdateAction()
              }}
            >
              <Form>
                <FormField label="Name" icons={[mdiBeakerQuestion]}>
                  <Field name="name" placeholder="name" />
                </FormField>

                <FormField label="Description" icons={[mdiAppleKeyboardOption]}>
                  <Field name="description" placeholder="description" />
                </FormField>

                <FormField label="Status" icons={[mdiBeakerQuestion]}>
                  <Field as="select" name="status">
                    <option key="0" value="TASK_STATUS_INVALID">
                      TASK_STATUS_INVALID
                    </option>
                    <option key="1" value="TASK_STATUS_INPROGRESS">
                      TASK_STATUS_INPROGRESS
                    </option>
                    <option key="2" value="TASK_STATUS_COMPLETED">
                      TASK_STATUS_COMPLETED
                    </option>
                    <option key="3" value="TASK_STATUS_CANCELLED">
                      TASK_STATUS_CANCELLED
                    </option>
                  </Field>
                </FormField>

                <FormField label="Priority" icons={[mdiBeakerQuestion]}>
                  <Field as="select" name="priority">
                    <option key="0" value="PRIORIRY_INVALID">
                      PRIORIRY_INVALID
                    </option>
                    <option key="1" value="PRIORIRY_LOW">
                      PRIORIRY_LOW
                    </option>
                    <option key="2" value="PRIORIRY_MEDIUM">
                      PRIORIRY_MEDIUM
                    </option>
                    <option key="3" value="PRIORIRY_HIGH">
                      PRIORIRY_HIGH
                    </option>
                  </Field>
                </FormField>

                <BaseButtons>
                  <BaseButton type="submit" color="info" label="Submit" />
                  <BaseButton type="reset" color="info" outline label="Reset" />
                </BaseButtons>
              </Form>
            </Formik>
          </CardBox>
        </CardBoxModal>

        {/* Search */}
        <CardBox>
          <Formik
            initialValues={{
              name: '',
              date: '',
            }}
            onSubmit={(values) => {
              const dateSearch = new Date(values.date)
              dateSearch.setHours(0)
              dateSearch.setMinutes(0)
              fetchData(values.name, Number.parseInt(values.date != '' ?  Math.floor(dateSearch.getTime()).toString() : '0'))
            }}
          >
            <Form>
              <FormField>
                <Field name="name" placeholder="Name" />
                <Field name="date" type="date" placeholder="Date" />
                <BaseButton type="submit" color="info" outline label="Search" />
              </FormField>
            </Form>
          </Formik>
        </CardBox>

        {/* List Todo */}
        <div className="grid grid-cols-1 gap-6 lg:grid-cols-3 mb-6">
          {tasks.map(function (task, i) {
            return (
              <a
                href="#"
                className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700"
              >
                <p className="font-normal text-gray-700 dark:text-gray-400">
                  Status Task: {task.status}
                </p>
                <p className="font-normal text-gray-700 dark:text-gray-400">
                  Priority: {task.priority}
                </p>
                <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                  Name: {task.name}
                </h5>
                <p className="font-normal text-gray-700 dark:text-gray-400">
                  Description: {task.description}
                </p>
                <BaseButtons>
                  <BaseButton
                    type="button"
                    color="info"
                    label="Update"
                    onClick={() => {
                      setTaskForUpdate(task)
                      setIsModalUpdateActive(true)
                    }}
                  />
                  <BaseButton
                    type="button"
                    color="danger"
                    label="Delete"
                    onClick={() => handleDelete(task.id)}
                  />
                </BaseButtons>
              </a>
            )
          })}
        </div>
      </SectionMain>
    </>
  )
}

Dashboard.getLayout = function getLayout(page: ReactElement) {
  return <LayoutAuthenticated>{page}</LayoutAuthenticated>
}

export default Dashboard
